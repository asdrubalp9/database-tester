<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        App\User::create([
            'name' => 'user',
            'email' => 'asd@asd.com',
            'password' => Hash::make('123'),
        ]);
        App\User::create([
            'name' => 'user2',
            'email' => 'qwe@qwe.com',
            'password' => Hash::make('123'),
        ]);
    }
}
